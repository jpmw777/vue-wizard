// The wizard configuration file
import wizardOptions from './wizard_options'

// Global settings for run-time env
const envSettings = {
  wizard_debug: false
}

// These get imported into the Vuex store
const settings = {
  APP_ID: 'wizard',
  KEY_SIGNATURES: {
    Db: 'D♭',
    Gb: 'G♭',
    Cb: 'C♭',
    Ab: 'A♭',
    Eb: 'E♭',
    Bb: 'B♭',
    F: 'F',
    C: 'C',
    G: 'G',
    D: 'D',
    A: 'A',
    E: 'E',
    B: 'B'
  },
  SAMPLE_LIST: [
    'Lorem',
    'ipsum',
    'sit',
    'amet',
    'consectetur',
    'adipiscing',
    'tincidun',
    'fermentum',
    'avaseptualario_boggie',
    'dictum'
  ]
}

export default {
  env_settings: envSettings,
  settings,
  wizard_options: wizardOptions
}
