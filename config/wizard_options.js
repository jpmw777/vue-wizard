const defs = {
  sample1: {
    name: 'sample1',
    title: 'Enter item text',
    forwardButtonText: 'OK',
    forwardButtonVisible: 0,
    okButtonText: 'Next',
    buttonBarVisible: 1,
    okButtonAction: 'appSetItem',
    onSelectOption: 'openImage',
    contentTitle: 'Enter an item',
    updateOptionArg: 0
  },
  sample2: {
    name: 'sample2',
    beforeLoading: 'appFetchSampleInfo',
    title: 'Show Item List',
    forwardButtonText: 'OK',
    forwardButtonVisible: 0,
    onSelectAction: 'appSelectListItem',
    contentTitle: 'Select item 1',
    buttonBarVisible: 1,
    updateOptionArg: 0
  },
  sample3: {
    name: 'sample3',
    buttonBarVisible: 1,
    title: 'Show entry',
    forwardButtonText: 'OK',
    forwardButtonVisible: 0,
    forwardButtonAction: 'wiz_SelectOption',
    onSelectAction: 'wiz_SelectOption',
    onSelectOption: 'openImage',
    contentTitle: 'Your entry:',
    updateOptionArg: 0
  },
  sample4: {
    name: 'sample4',
    buttonBarVisible: 1,
    title: 'Select A Key',
    forwardButtonVisible: 0,
    onSelectAction: 'appSelectItem',
    contentTitle: 'Select from the list',
    selectSource: 'KEY_SIGNATURES'
  },
  sample5: {
    name: 'sample5',
    beforeLoading: 'appBuildWizardResult',
    buttonBarVisible: 1,
    title: 'We are done',
    forwardButtonAction: 'appProcessSampleData',
    forwardButtonText: 'Finish',
    forwardButtonVisible: 1,
    contentTitle: 'Wizard Demo'
  },
  about: {
    name: 'about',
    title: 'About',
    contentTitle: 'Program Information',
    beforeLoading: 'systemAbout',
    forwardButtonText: 'OK',
    forwardButtonAction: 'wizard/wizard_Hide'
  },
  changePassword: {
    name: 'changePassword',
    title: 'Change Password',
    forwardButtonAction: 'userChangePassword',
    contentTitle: 'Change Password',
    forwardButtonVisible: 1,
    forwardButtonText: 'Change'
  },
  resetPassword: {
    name: 'resetPassword',
    title: 'Reset Password',
    contentTitle: 'Reset Password',
    forwardButtonAction: 'userResetPassword',
    forwardButtonVisible: 1,
    forwardButtonText: 'Reset'
  },
  logout: {
    name: 'logOut',
    title: 'Log Out',
    contentTitle: 'Log Out',
    forwardButtonAction: 'userLogout',
    forwardButtonVisible: 1,
    forwardButtonText: 'Log Out'
  }
}

const optionLists = {
  tasks: {
    title: 'Tasks',
    options: ['sample1', 'sample2', 'sample3', 'sample4', 'sample5']
  },
  user: {
    title: 'User',
    options: ['changePassword', 'resetPassword', 'logout']
  },
  tools: {
    title: 'Tools',
    options: ['about']
  }
}

// Startup conditions
const initial = {
  base: 'tools',
  index: 0
}

export default {
  defs,
  lists: optionLists,
  init: initial
}
