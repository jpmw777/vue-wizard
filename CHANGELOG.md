
## Name: Vue JS Wizard Demo
## Description: Vue Wizard: Vuex version
## Author: Medur C Wilson

## v0.1.3
================
- Changed: Updated Readme
  Uploaded to Gitlab

## v0.1.2
================
- Changed: src directory

## v0.1.1
================
- Changed: Logo image
  css styles

## v0.1.0
================
- Fixed: harmonized button styling

## v0.0.5
================
- Fixed: wizard demo options

## v0.0.4
================
- Fixed: wizard module namespacing
  Set up demo: tasks

## v0.0.3
================
- Fixed: wizard store actions/dispatcher

## v0.0.2
================
- Added wizard store

## v0.0.1
================
- Created empty project
- Added vuex store

