export default {
  // APP:
  appFetchSampleInfo(c, args) {
    const key = c.getters['wizard/getWizard_ActiveId']
    const list = c.getters.appConfig.settings.SAMPLE_LIST
    const contentFn = args.woption.setContent
    c.commit(contentFn, { key, value: list })
  },

  appBuildWizardResult(c, args) {
    const list = []
    const selections = c.getters['wizard/getWizard_Selections']
    list.push('Your text: ' + selections.tasks_0)
    list.push('Your selection: ' + selections.tasks_1)
    list.push('In the key of: ' + selections.tasks_3)
    c.commit('wizard/WIZARD_SET_CURRENT_SELECTION', '\n' + list.join('\n'))
  },

  appProcessSampleData(c, args) {
    alert('Close the wizard, then process the wizard data.')
    const data = c.getters['wizard/getWizard_CurrentSelection']
    c.commit('SET_SAMPLE_CONTENT', data)
    c.dispatch('wizard/wizard_Hide')
  },

  // App specific startup routine
  appInit: (c, args) => {
    c.commit('SET_FORM_CONTENT', {
      key: 'tasks_1',
      value: c.getters.appConfig.settings.SAMPLE_LIST
    })
  },

  appSelectListItem(c, args) {
    c.commit('wizard/WIZARD_MOVE', 1)
    c.commit('SET_SAMPLE_CONTENT', args.payload.item)
  },

  userChangePassword(c) {
    c.dispatch('wizard/wizard_Hide')
    c.commit('SET_SAMPLE_CONTENT', 'Password Changed!')
  },

  userResetPassword(c) {
    c.dispatch('wizard/wizard_Hide')
    c.commit('SET_SAMPLE_CONTENT', 'Password Reset!')
  },

  userLogout(c) {
    c.dispatch('wizard/wizard_Hide')
    c.commit('SET_SAMPLE_CONTENT', 'Logged Out!')
  },

  appSetItem(c, args) {
    //    c.commit('WIZARD_TEST', args)
    c.commit('SET_SAMPLE_CONTENT', args.payload.item)
    // c.dispatch('wizard_SetNextOption', args.woption)
    c.dispatch('wizard/wizard_SetNextOption', args.woption)
  },

  appSelectItem(c, args) {
    c.commit('SET_SAMPLE_CONTENT', args.payload.item)
  },

  // Execute this action when app is first loaded
  systemInit: (c) => {
    c.dispatch('appInit')
  },

  systemAbout(c) {
    c.commit('wizard/WIZARD_SET_CURRENT_SELECTION_ID', 0)
    const text = 'Wizard Demo Version 0.0.1'
    c.commit('wizard/WIZARD_SET_CURRENT_SELECTION', text)
  },

  appToggleBodyClass(c, args) {
    const el = document.body
    if (args.addRemoveClass === 'addClass') {
      el.classList.add(args.className)
    } else {
      el.classList.remove(args.className)
    }
  }
}
