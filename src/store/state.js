import Config from '../../config'

export default () => ({
  // APP
  appId: 'APP_ID',

  appSampleContent: 'Hello, This is the wizard demo sample content data.',

  // CONFIG:
  appConfig: Config,

  // SYSTEM:
  systemEnv: 'process.env.NODE_ENV',

  formContent_tools_0: [],
  formContent_tasks_0: [],
  formContent_tasks_1: [],
  formContent_tasks_3: []
})
