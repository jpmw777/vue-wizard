import Config from '../../../config'
import classes from './class'

export default () => ({
  wizard_Active: new classes.ClassWizardOption(),
  wizard_ActiveBase: Config.wizard_options.init.base,
  wizard_ActiveOptionNumber: Config.wizard_options.init.index,
  wizard_Class: classes.classWizardOption,
  wizard_CurrentSelectionId: -1,
  wizard_CurrentSelection: '',
  wizard_CurrentSelectionData: {},
  wizard_Debug: Config.env_settings.wizard_debug,
  wizard_Defs: Config.wizard_options.defs,
  wizard_Init: Config.wizard_options.init,
  wizard_Lists: Config.wizard_options.lists,
  wizard_MoveDown: 0,
  wizard_OptionArgs: {},
  wizard_Show: 0,
  wizard_Selections: {},
  wizard_Test: ''
})
