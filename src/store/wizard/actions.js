import classes from './class'

export default {
  wizard_ToggleDebug: (c) => {
    const debug = c.getters.getWizard_Debug
    c.commit('WIZARD_SET_DEBUG', !debug)
  },
  wizard_Show: (c, show) => {
    c.commit('WIZARD_SHOW', show)
  },
  wizard_Hide: (c) => {
    c.commit('WIZARD_SHOW', 0)
    // c.commit('SET_INFO_POPUP_VISIBILITY', false)
  },
  wizard_Test: (c, payload) => {
    c.commit('WIZARD_TEST', payload)
  },
  wizard_SetCurrentSelection: (c, value) => {
    c.commit('WIZARD_SET_CURRENT_SELECTION', value)
  },

  // The first step in starting the wizard
  wizard_Open: (c, options) => {
    // The requesting object with the parameters specifying which wizard to load
    const wizardRequest = validateWizardOption(c, options)

    // Store these parameters for later use (much use)
    c.commit('WIZARD_SET_ACTIVE_BASE', wizardRequest.base)
    c.commit('WIZARD_SET_ACTIVE_OPTION_NUMBER', wizardRequest.index || 0)

    // Reset the wizard's position
    c.commit('WIZARD_SHOW', 1)

    // The skeleton object verbatim from the wizard_options config file
    const wizard = c.getters.getWizard_ActiveOptionDef

    if (wizardRequest.contentTitle.length) {
      wizard.contentTitle = wizardRequest.contentTitle
    }
    // Enclose the runtime payload coming from the client
    if (!options) {
      wizard.payload = {}
    } else {
      wizard.payload = options.payload || options || {}
    }

    // Set the wizard in motion!
    c.dispatch('wizard_Set', wizard).then((newWizard) => {
      // There is a new wizard on the block, commit it to vuex
      c.commit('WIZARD_SET_ACTIVE', newWizard)
    })
  },

  // The second step in starting the wizard, separated from the first part
  // because this routine can be called from several points in the calling app
  // or elsewhere in the wizard infrastructure, either programmatically
  // or as a result of a user-triggered event
  wizard_Set: (c, wizardRequest = {}) => {
    // Close competing features
    // c.commit('SET_INFO_POPUP_VISIBILITY', false)

    // Get the current active wizard
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)
    const id = c.getters.getWizard_ActiveId
    const op = wizardRequest || wizard || c.getters.getWizard_OptionDefs[id]

    // set the new wizard from the wizard object's initialization method
    return wizard.setWizard(c, op).then((newWizard) => {
      // Dev only
      if (c.getters.getWizard_Debug) {
        c.commit('WIZARD_TEST', newWizard)
      }
      return newWizard
    })
  },

  // The left button on the WizardFooter, usually for `cancel` or `back`
  wizard_PrevButtonClick: (c, options = {}) => {
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)
    wizard.payload = options.payload || options || {}

    // Execute any actions as specified in the config options
    const op = wizard.dispatcher(c, wizard, 'prevButtonAction')
    c.dispatch(op.f, op.a, { root: true })
  },

  // This is called when an option is selected programmatically
  wizard_SelectOption: (c, options) => {
    // close down competing features
    // c.commit('SET_INFO_POPUP_VISIBILITY', false)
    let wizard = options.woption || c.getters.getWizard_Active

    // Load the new option as specified in the wizard config
    wizard = c.getters.getWizard_OptionDefs[wizard.onSelectOption]
    wizard.payload = options.payload || {}
    c.dispatch('wizard_Set', wizard).then((newWizard) => {
      c.commit('WIZARD_SET_ACTIVE', newWizard)
    })
  },
  // Called when an item in a wizard view is selected
  wizard_SelectItem: (c, options) => {
    const key = c.getters.getWizard_ActiveId
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)

    // These settings are used by the parent app invoking the wizard
    c.commit('WIZARD_SET_CURRENT_SELECTION_ID', options.index)
    c.commit('WIZARD_SET_CURRENT_SELECTION', options.item)
    c.commit('WIZARD_SET_CURRENT_SELECTION_DATA', options.data || {})
    c.commit('WIZARD_SET_SELECTION', { key, value: options.item })

    wizard.payload = options.payload || options
    if (wizard.updateOptionArg) {
      c.commit('WIZARD_SET_OPTION_ARG', { key, value: options.item })
    }
    const fn = options.fn || ''
    const op = wizard.dispatcher(c, wizard, fn)
    if (op.f) {
      c.dispatch(op.f, op.a, { root: true })
    }
  },

  // This is called when a user selects an option
  wizard_OptionSelectClick: (c, options) => {
    options.base = c.getters.getWizard_ActiveBase
    const wizardOption = validateWizardOption(c, options)

    c.commit('WIZARD_SET_ACTIVE_OPTION_NUMBER', wizardOption.index || 0)
    let wizard = c.getters.getWizard_ActiveOptionDef
    wizard = c.getters.getWizard_OptionDefs[options.option] || wizard
    const id = c.getters.getWizard_ActiveId

    if (!options) {
      wizard.payload = {}
    } else {
      wizard.payload = options.payload || options || {}
    }

    // Carry forward the previous value or set the default
    let value = c.getters.getWizard_OptionArg(id) || wizard.defaultContent || ''

    if (Object.keys(c.getters.getWizard_Selections).includes(id)) {
      value = c.getters.getWizard_Selections[id]
    }
    c.commit('WIZARD_SET_SELECTION', { key: id, value })
    c.commit('WIZARD_SET_CURRENT_SELECTION_ID', 0)
    c.commit('WIZARD_SET_CURRENT_SELECTION', value)
    c.dispatch('wizard_Set', wizard).then((newWizard) => {
      c.commit('WIZARD_SET_ACTIVE', newWizard)
    })
  },

  wizard_OkButtonClick: (c, options = {}) => {
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)
    // Destructure the payload, as it may be nested inside the options object
    // depending on where the options object was last updated
    const payload = options.payload || options || {}
    payload.item = payload.item || c.getters.getWizard_CurrentSelection
    payload.data = payload.data || c.getters.getWizard_CurrentSelectionData
    payload.index = payload.index || c.getters.getWizard_CurrentSelectionId

    wizard.payload = payload
    if (wizard.okButtonOptionBase) {
      c.dispatch('wizard_SetOptionBase', {
        base: wizard.okButtonOptionBase,
        option: 0
      }).then(() => {
        const optionDef = c.getters.getWizard_ActiveOptionDef
        optionDef.payload = payload
        c.dispatch('wizard_Set', optionDef).then((newWizard) => {
          c.commit('WIZARD_SET_ACTIVE', newWizard)
        })
      })
    } else if (
      wizard.forwardButtonAction === '__NEXT__' ||
      wizard.okButtonAction === '__NEXT__'
    ) {
      c.dispatch('wizard_SetNextOption', wizard)
    } else if (wizard.okButtonAction) {
      const op = wizard.dispatcher(c, wizard, 'okButtonAction')

      c.dispatch(op.f, op.a, { root: true })
    }
  },

  wizard_SetNextOption: (c, options) => {
    const key = c.getters.getWizard_ActiveId
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)
    const op = options || {}
    const payload = op.payload || options || {}
    c.commit('WIZARD_SET_SELECTION', { key, value: payload.item || '' })
    if (wizard.updateOptionArg) {
      c.commit('WIZARD_SET_OPTION_ARG', {
        key,
        value: payload.item || ''
      })
    }
    let optionCount = c.getters.getWizard_OptionList(
      c.getters.getWizard_ActiveBase
    )
    optionCount = optionCount.length
    let nextOption = c.getters.getWizard_ActiveOptionNumber + 1
    if (nextOption >= optionCount) {
      nextOption = 0
    }
    c.commit('WIZARD_SET_ACTIVE_OPTION_NUMBER', nextOption)

    const newOptionDef = c.getters.getWizard_ActiveOptionDef
    const newKey = c.getters.getWizard_ActiveId
    // Carry forward the previous value or set the default
    let value = newOptionDef.defaultContent || ''
    if (wizard.nextOptionValue) {
      value = options.item || ''
    }
    if (Object.keys(c.getters.getWizard_Selections).includes(newKey)) {
      if (c.getters.getWizard_Selections[newKey]) {
        value = c.getters.getWizard_Selections[newKey]
      }
    }
    c.commit('WIZARD_SET_SELECTION', { key: newKey, value })
    c.commit('WIZARD_SET_CURRENT_SELECTION_ID', 0)
    c.commit('WIZARD_SET_CURRENT_SELECTION', value)
    newOptionDef.payload = wizard.payload
    c.dispatch('wizard_Set', newOptionDef).then((newWizard) => {
      c.commit('WIZARD_SET_ACTIVE', newWizard)
    })
  },

  // Action executed when forward button is clicked
  wizard_ForwardButtonClick: (c, args = {}) => {
    // Cast the options into a new object
    const options = JSON.parse(JSON.stringify(args))
    // Load the wizard object
    const wizard = new classes.ClassWizardOption(c.getters.getWizard_Active)

    wizard.payload = options.payload || {}
    const wId = c.getters.getWizard_ActiveId

    // Update the payload with the current selections
    wizard.payload.item = c.getters.getWizard_CurrentSelection
    wizard.payload.index = c.getters.getWizard_CurrentSelectionId

    // Prepare for shift to new set of options, if this is set in the wizard
    const newBase = wizard.forwardButtonOptionBase || ''

    if (newBase) {
      // Get the preset value for the wizard's current options
      const optionArg = c.getters.getWizard_OptionArg(wId)
      options.base = newBase
      options.index = wizard.forwardButtonOptionNum || 0
      // Set up the new option base
      c.dispatch('wizard_SetOptionBase', options)
        .then(() => {
          // Record the option preset values for later retrieval
          c.commit('WIZARD_SET_OPTION_ARG', { key: wId, value: optionArg })
        })
        .then(() => {
          const newOptionDef = c.getters.getWizard_ActiveOptionDef
          newOptionDef.payload = wizard.payload || {}
          // Set up the new wizard
          c.dispatch('wizard_Set', newOptionDef).then((newWizard) => {
            c.commit('WIZARD_SET_ACTIVE', newWizard)
          })
          // Execute any actions as required by the wizard's configuration
          const op = wizard.dispatcher(c, wizard, 'forwardButtonAction')
          if (op.f) {
            c.dispatch(op.f, op.a, { root: true })
          }
        })
    } else if (wizard.forwardButtonAction === '__NEXT__') {
      // Go to the next option in the list as the default action
      // of the OkButton
      c.dispatch('wizard_SetNextOption', wizard)
    } else {
      // The action defined for this wizard when the button is clicked
      const op = wizard.dispatcher(c, wizard, 'forwardButtonAction')
      if (op.f) {
        c.dispatch(op.f, op.a, { root: true })
      }
    }
  },

  wizard_ClearOptionBaseSelections: (c, args) => {
    const base = args.base || ''
    if (!base) return
    const selections = Object.keys(c.getters.getWizard_Selections)
    selections.forEach((selectionKey) => {
      const sbase = selectionKey.split('_').shift()
      if (sbase === base) {
        c.commit('WIZARD_SET_SELECTION', { key: selectionKey, value: '' })
      }
    })
  },
  wizard_SetOptionBase: (c, args) => {
    const obase = args.base || ''
    if (!obase) return
    const target = obase.split('?')
    const base = target.shift()
    c.commit('WIZARD_SET_ACTIVE_BASE', base)
    c.commit('WIZARD_SET_ACTIVE_OPTION_NUMBER', args.index)
    c.commit('WIZARD_SET_CURRENT_SELECTION_ID', -1)
    let directives = target.shift()
    if (!directives) return
    directives = c.getters.systemUtils.urlParseQueryString(directives)
    Object.keys(directives).forEach((key) => {
      switch (key) {
        case 'C':
          c.dispatch('wizard_ClearOptionBaseSelections', { base })
          break
        case 'X':
          c.dispatch(directives.X, args.payload || args, { root: true })
      }
    })
  },

  wizard_SetDefaultValue: (c) => {
    const index = c.getters.getWizard_ActiveId
    const op = c.getters.getWizard_Active
    c.commit('WIZARD_SET_SELECTION', { key: index, value: op.defaultContent })
  }
}

function validateWizardOption(c, option) {
  // Determine which wizard option is requested
  // and ensure that it is compliant and properly structured
  const requestedWizardOption = {}
  // Use the provided one or the default
  const optionBase = option || c.getters.getWizard_Init
  // Extract the attributes
  const baseName = optionBase.base || ''
  let optionNumber = optionBase.index || 0
  const optionName = optionBase.option || ''

  requestedWizardOption.contentTitle = optionBase.contentTitle || ''

  // validate:
  const optionList = c.getters.getWizard_OptionList(baseName)
  if (optionList.length > 0) {
    requestedWizardOption.base = baseName
    requestedWizardOption.index = optionNumber
    requestedWizardOption.name = optionName
    // Let the named option over-ride the requested index - if validated
    if (optionName) {
      optionNumber = c.getters
        .getWizard_OptionList(baseName)
        .indexOf(optionName)
      if (optionNumber >= 0) {
        requestedWizardOption.index = optionNumber
      } else {
        requestedWizardOption.name = ''
      }
    }
  }
  return requestedWizardOption
}
