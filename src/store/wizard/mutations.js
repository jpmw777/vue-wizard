export default {
  WIZARD_SET_DEBUG(st, arg) {
    st.wizard_Debug = !!arg
  },
  WIZARD_TEST(st, test) {
    st.wizard_Test = test
  },
  WIZARD_MOVE(st, move) {
    st.wizard_MoveDown = move ? 1 : 0
  },
  WIZARD_SHOW(st, show) {
    st.wizard_Show = show ? 1 : 0
  },
  WIZARD_SET_ID(st, id) {
    st.wizard_Id = id
  },
  WIZARD_SET_ACTIVE(st, wizard) {
    st.wizard_Active = wizard
  },
  WIZARD_SET_ACTIVE_BASE(st, base) {
    st.wizard_ActiveBase = base.base || base
    const index = base.index || 0
    st.wizard_ActiveOptionNumber = index
  },
  WIZARD_SET_ACTIVE_OPTION_NUMBER(st, n) {
    st.wizard_ActiveOptionNumber = n
  },
  WIZARD_SET_OPTION_ARG(st, { key, value }) {
    st.wizard_OptionArgs[key] = value
  },
  // the index of the selected item in a select list || OR "0"
  WIZARD_SET_CURRENT_SELECTION_ID(st, index) {
    st.wizard_CurrentSelectionId = index
  },
  WIZARD_SET_CURRENT_SELECTION(st, value) {
    st.wizard_CurrentSelection = value
  },
  WIZARD_SET_CURRENT_SELECTION_DATA(st, data) {
    st.wizard_CurrentSelectionData = data
  },
  WIZARD_SET_SELECTION(st, { key, value }) {
    // Cast the selections into a new object
    const selections = JSON.parse(JSON.stringify(st.wizard_Selections))
    selections[key] = value
    st.wizard_Selections = selections
  },
  WIZARD_CLEAR_SELECTION(st) {
    st.wizard_Selections = {}
  }
}
