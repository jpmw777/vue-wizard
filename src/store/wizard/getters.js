export default {
  // WIZARD
  getWizard_Active: (st) => st.wizard_Active,
  getWizard_ActiveId: (st) =>
    st.wizard_ActiveBase + '_' + st.wizard_ActiveOptionNumber,
  getWizard_ActiveBase: (st) => st.wizard_ActiveBase,
  getWizard_ActiveBaseTitle: (st) =>
    st.wizard_Lists[st.wizard_ActiveBase].title,
  getWizard_ActiveOptionNumber: (st) => st.wizard_ActiveOptionNumber,
  getWizard_ActiveOptionDef: (st) => {
    // Get the requested wizard option from the wizard_options config object
    const optionList = st.wizard_Lists[st.wizard_ActiveBase]
    if (!optionList) return st.wizard_Active
    const option =
      optionList.options[st.wizard_ActiveOptionNumber] || optionList[0]
    return st.wizard_Defs[option] || st.wizard_Active
  },
  getWizard_Class: (st) => st.wizard_Class,
  getWizard_Debug: (st) => st.wizard_Debug,
  getWizard_Init: (st) => st.wizard_Init,
  getWizard_OptionDefs: (st) => st.wizard_Defs,
  getWizard_OptionLists: (st) => st.wizard_Lists,
  getWizard_OptionList: (st) => (base) => {
    if (Object.keys(st.wizard_Lists).includes(base)) {
      return st.wizard_Lists[base].options
    } else {
      return []
    }
  },
  getWizard_OptionArg: (st) => (id) => st.wizard_OptionArgs[id],
  getWizard_OptionArgs: (st) => st.wizard_OptionArgs,
  getWizard_Show: (state) => state.wizard_Show,
  getWizard_Label: (st) => st.wizard_Active.label,
  getWizard_MoveDown: (state) => state.wizard_MoveDown,
  getWizard_Test: (st) => st.wizard_Test,
  getWizard_CurrentSelectionId: (st) => st.wizard_CurrentSelectionId,
  getWizard_CurrentSelection: (st) => st.wizard_CurrentSelection,
  getWizard_CurrentSelectionData: (st) => st.wizard_CurrentSelectionData,
  getWizard_Selections: (st) => st.wizard_Selections
}
