class WizardOption {
  constructor({
    name = '',
    title = '',
    contentTitle = '',
    contentCode = '',
    beforeLoading = '',
    // option to set on selection
    onSelectOption = '',
    // function to call on selection
    onSelectAction = '',
    prevButtonText = 'Cancel',
    prevButtonVisible = 1,
    prevButtonAction = 'wizard/wizard_Hide',
    forwardButtonText = 'Next',
    forwardButtonVisible = 0,
    forwardButtonAction = '',
    forwardButtonOptionBase = '',
    forwardButtonOptionNum = 0,
    buttonBarVisible = 0,
    okButtonText = 'Next',
    okButtonAction = '__NEXT__',
    okButtonOptionBase = '',
    nextOptionValue = 0,
    setContent = 'SET_FORM_CONTENT',
    defaultContent = '',
    selectSource = '',
    beforeDeparture = '',
    updateOptionArg = 1,
    label = '',
    payload = {}
  } = {}) {
    this.name = name
    this.title = title
    this.contentTitle = contentTitle
    this.contentCode = contentCode
    this.beforeLoading = beforeLoading
    this.onSelectOption = onSelectOption
    this.onSelectAction = onSelectAction
    this.prevButtonText = prevButtonText
    this.prevButtonVisible = prevButtonVisible
    this.prevButtonAction = prevButtonAction
    this.forwardButtonText = forwardButtonText
    this.forwardButtonVisible = forwardButtonVisible
    this.forwardButtonAction = forwardButtonAction
    this.forwardButtonOptionBase = forwardButtonOptionBase
    this.forwardButtonOptionNum = forwardButtonOptionNum
    this.buttonBarVisible = buttonBarVisible
    this.okButtonText = okButtonText
    this.okButtonAction = okButtonAction
    this.okButtonOptionBase = okButtonOptionBase
    // set next option value from current
    this.nextOptionValue = nextOptionValue
    this.setContent = setContent
    this.defaultContent = defaultContent
    this.selectSource = selectSource
    this.beforeDeparture = beforeDeparture
    this.updateOptionArg = updateOptionArg
    this.label = label
    this.payload = payload
  }

  async setWizard(control, op = {}) {
    const option = op || this
    const newWizard = new this.constructor(option)
    // execute departure actions on active wizard option
    let disp = this.dispatcher(control, option, 'beforeDeparture')
    // execute departure actions on active wizard option
    if (disp.f) {
      await control.dispatch(disp.f, disp.a, { root: true })
    }

    // execute beforeLoading actions on new wizard option
    disp = this.dispatcher(control, newWizard, 'beforeLoading')
    if (disp.f) {
      await control.dispatch(disp.f, disp.a, { root: true })
    }
    return newWizard
  }

  dispatcher(control, options = {}, fn = 'beforeLoading') {
    const args = this.prepareArgs(control, options, fn)

    if (!args.fn) return {}
    return { f: args.fn, a: args }
  }

  prepareArgs(control, options = {}, fn = 'beforeLoading') {
    const args = {}
    if (!options) return args
    if (!fn) return args

    fn = options[fn]
    if (!fn) return args

    args.wizard_selections = {}
    args.payload = options.payload || options
    args.event = options.e || {}
    args.woption = new this.constructor(options)
    args.fn = fn
    return args
  }
}

export default { ClassWizardOption: WizardOption }
