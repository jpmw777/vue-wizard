export default {
  // APP:
  appId: (st) => st.appId,
  appSampleContent: (st) => st.appSampleContent,

  appFormContentValue: (st) => (key) => {
    return st['formContent_' + key]
  },

  // CONFIG:
  appConfig: (st) => st.appConfig,

  // SYSTEM:
  systemEnv: (st) => st.systemEnv,
  systemEnvIsProduction: (st) => st.systemEnv === 'production'
}
