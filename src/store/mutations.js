export default {
  // APP:
  SET_FORM_CONTENT: (st, { key, value }) => {
    st['formContent_' + key] = value
  },

  SET_SAMPLE_CONTENT: (st, content) => {
    st.appSampleContent = content
  }
}
